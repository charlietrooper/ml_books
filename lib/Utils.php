<?php
/**
 * Created by PhpStorm.
 * User: AppStudio
 * Date: 27/06/2015
 * Time: 11:30 AM
 */
use Prewk\XmlStringStreamer;
use Prewk\XmlStringStreamer\Parser\StringWalker;
use Prewk\XmlStringStreamer\Stream\File;


class Utils{

    /**
     * Limpia el string que obtenemos de la llamada a json_encode al transformar el xml a doc json
     * Es necesario quitar el inico y fin de la cadena para simplificar la jerarquia del documento
     * Lo que se elimina es "book":{ <datos_se_mantienen> }
     */
    public static function cleanJSON($json_string){

        $json_string = substr($json_string, 8, -1);
        return $json_string;

    }

    /**
     * Obtiene y limpia el JSON final para insertar el ebook como documento
     * Limpia y agrega los campos de _id y lastUpdate
     */
    public static function getBookJSONFromXMLNode($node){

        //Funcion de procesamiento del XMl a la cadena JSON
        $arr = xml2json::transformXmlStringToArray($node);
        //concvert to json
        $jsonContents = json_encode($arr);

        //Eliminación de datos innecesarios en la cadena JSON
        $json_string = Utils::cleanJSON($jsonContents);

        return $json_string;

    }

    /**
     * Obtiene y limpia el JSON final para insertar el ebook como documento
     * Limpia y agrega los campos de _id y lastUpdate
     */
    public static function getBookJSONFromXMLNode2($node){

        //Funcion de procesamiento del XMl a la cadena JSON
        $arr = XML2Array::createArray($node);

        //concvert to json
        $jsonContents = json_encode($arr);

        //Eliminación de datos innecesarios en la cadena JSON
        $json_string = Utils::cleanJSON($jsonContents);

        return $json_string;

    }

    /**
     *
     */
    public static function convertPendingXMLtoJSON($filename, $logger){
        //Number of processed nodes (books)
        $count = 0;

        //Nombre del archivo local
        $local_file = DATA_PENDING_DIR . $filename;

        //Nombre del archivo final
        $json_file = DATA_OUTPUT_DIR . "$filename.json";

        //Metodo para la obtención del tamaño del archivo XML local
        $totalSize = filesize($local_file);

        $start_timestamp = date('Y-m-d H:i:s');

        // Se prepara el streaming y monitoreo con 16kb de buffer
        $progress = 0;
        $last_progress = 0;
        $stream = new File($local_file, 16384, function ($chunk, $readBytes) use ($progress, &$last_progress, $totalSize, $logger) {

            $progress =  $readBytes / $totalSize;

            //report every 10%
            if($progress >= $last_progress + 0.1){
                $logger->log("Progress: $progress");
                $last_progress = $last_progress + 0.1;
            }
        });

        $start_timestamp = date('Y-m-d H:i:s');
        //Configura el parser

        $parser = new StringWalker;

        //Configura el streamer
        $streamer = new XmlStringStreamer($parser, $stream);

        //Creación del archivo final
        $file = fopen($json_file, "w") or die(json_encode("Could not open {$json_file} for writing"));

        $logger->log("Convirtiendo {$local_file} a {$json_file}...");
        //Procesamiento de nodos
        while ($node = $streamer->getNode()) {

            //Set json string ready for mongo insertion
            $json_string = Utils::getBookJSONFromXMLNode($node);

            //Inserta la cadena en el archivo final
            fputs($file, $json_string . PHP_EOL);

            $count++;
        }

        if($count == 0){
            $logger->error("0 Records converted");
        }else{
            $logger->log("$count Records converted");
        }

        //Cierra la edición del archivo final
        fclose($file);

        //Elimina la cache del proceso
        clearstatcache();

        return $count;

    }

    public static function slashString($content,$start,$end){
        $str = explode($start, $content);
        if (isset($str[1])){
            $str = explode($end, $str[1]);
            return $str[0];
        }
        return '';
    }

}