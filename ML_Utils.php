<?php
require 'vendor/autoload.php';
require 'src/utils_config.php';
include('src/autoload.php');
include('lib/XmlStringStreamer.php');
include('lib/Array2XML/Array2XML.php');
include('lib/Array2XML/XML2Array.php');
include('lib/Utils.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class MLUtils
{
    //Función para descargar el catálogo general de Libranda
    public static function librandaOnixCatalog()
    {
        $pagination_counter = 1;
        $url = WEBCATALOG;
        $date = new DateTime();
        $start_date = $date->format("y-m-d h:i:s");
        //De acuerdo a la cantidad de libros total, deberan cambiar el counter al total requerido
        while ($pagination_counter <= 1400) {
            $myxmlfilecontent = file_get_contents($url . $pagination_counter);
            if ($myxmlfilecontent === FALSE) {
                $log = "Start Time: " . $start_date . PHP_EOL . "Error: File not found ";
                file_put_contents("log/errorlog-" . $start_date . ".log", $log, FILE_APPEND);
                break;
            } else {
                file_put_contents("onix/general/onix-" . $pagination_counter . ".onix", $myxmlfilecontent, FILE_APPEND);
                $pagination_counter++;
            }

        }
        $date_2 = new DateTime();
        $final_date = $date_2->format("y-m-d h:i:s");
        $interval = date_diff($date, $date_2);
        $diff = $interval->format('%h:%i:%s');
        $log = "Start Time: " . $start_date . PHP_EOL . "Final Time: " . $final_date . PHP_EOL . "Difference: " . $diff;
        file_put_contents("log/generalonixlog-" . $start_date . ".log", $log, FILE_APPEND);
    }

    public static function readDirectory($directory)
    {
        $allFiles = scandir($directory);
        $files = array_diff($allFiles, array('.', '..'));
        //Regresa los archivos sin los excedentes del directorio
        return $files;
    }

    //Función para bajar las deltas del ftp
    public static function ftpOnixCatalog()
    {
        $ml = new MLUtils();
        $date = new DateTime();
        $start_date = $date->format("y-m-d h:i:s");
        $sftp = new \phpseclib\Net\SFTP(FTPONIXCATALOG, 2222);
        if (!$sftp->login(FTPUSERONIXCATALOG, FTPWDONIXCATALOG)) {
            $log = "Start Time: " . $start_date . PHP_EOL . "Error: Login Failed ";
            file_put_contents("log/errorlog-" . $start_date . ".log", $log, FILE_APPEND);
            exit('Login Failed');
        }
        $sftp_files = $sftp->nlist();
        $files = array_diff($sftp_files, array('.', '..', 'assets'));
        foreach ($files as $file) {
            $sftp->get($file, 'onix/sftp/' . $file);
        }
        $directory = 'onix/sftp';
        $bzips = $ml->readDirectory($directory);
        foreach ($bzips as $bzip) {
            //Descomprime los archivos
            bzdecompress($directory . "/" . $bzip);
            //Elimina los archivos bz2 para liberar espacio, dejando solo los onix
            if (pathinfo($directory . "/" . $bzip, PATHINFO_EXTENSION) == 'bz2') {
                unlink($directory . "/" . $bzip);
            }
        }
        $date_2 = new DateTime();
        $final_date = $date_2->format("y-m-d h:i:s");
        $interval = date_diff($date, $date_2);
        $diff = $interval->format('%h:%i:%s');
        $log = "Start Time: " . $start_date . PHP_EOL . "Final Time: " . $final_date . PHP_EOL . "Difference: " . $diff;
        file_put_contents("log/ftponixlog-" . $start_date . ".log", $log, FILE_APPEND);
    }

    //Función de conversión del Object de Onix a Array. Tiene como variable $product que deriva del parser de XML a Object para los Onix
    public static function objectToArray($product)
    {
        if (!is_object($product) && !is_array($product)) return $product;
        //Regresa el array del librop
        return array_map('self::objectToArray', (array)$product);
    }

    //Función de conversión del Array de Onix a un Json para MongoDB. Tiene la variable $directory para darle la ubicación de los archivos Onix a convertir a json
    public static function jsonCreation($directory)
    {
        $ml = new MLUtils();
        $date = new DateTime();
        $start_date = $date->format("y-m-d h:i:s");
        $onix_files = $ml->readDirectory($directory);
        foreach ($onix_files as $onix) {
            $parser = New BBMParser\OnixParser($directory . "/" . $onix, true);
            $products = $parser->getOnix()->getProducts();
            foreach ($products as $product) {
                $array_product = $ml->objectToArray($product);
                $json_onix = json_encode($array_product);
                //$final_json = $json_onix.'{"provider":"libranda"}';
                $filename = basename($onix, ".onix");
                //CReación del archivo json con el libro extraido. Un libro en json por linea en el archivo
                file_put_contents("json/" . $filename . ".json", $json_onix . PHP_EOL, FILE_APPEND);
            }
        }
        $date_2 = new DateTime();
        $final_date = $date_2->format("y-m-d h:i:s");
        $interval = date_diff($date, $date_2);
        $diff = $interval->format('%h:%i:%s');
        $log = "Start Time: " . $start_date . PHP_EOL . "Final Time: " . $final_date . PHP_EOL . "Difference: " . $diff;
        file_put_contents("log/jsoncreationlog-" . $start_date . ".log", $log, FILE_APPEND);
    }

    //Función de importación de los json de Onix a Mongo
    public static function jsonImporttoMongo()
    {
        $ml = new MLUtils();
        $date = new DateTime();
        $start_date = $date->format("y-m-d h:i:s");
        $json_dir = $ml->readDirectory('json');
        foreach ($json_dir as $json_file) {
            //En el comando se deberá cambiar el host, base de datos, colección y directorio de los json (de forma normal, no absoluta)
            $mongo_command = MONGOCOMMAND.$json_file;
            //Ejecuta el comando de importación, trayendo un resultado
            exec($mongo_command);
        }
        $date_2 = new DateTime();
        $final_date = $date_2->format("y-m-d h:i:s");
        $interval = date_diff($date, $date_2);
        $diff = $interval->format('%h:%i:%s');
        $log = "Start Time: " . $start_date . PHP_EOL . "Final Time: " . $final_date . PHP_EOL . "Difference: " . $diff;
        file_put_contents("log/jsontomongolog-" . $start_date . ".log", $log, FILE_APPEND);
    }

    //Función para realizar la conexión con Mongo para usar las funciones a futuro del catálogo. Tiene la variable $collection para decidir que colección de Mongo quieres usar
    public static function mongoDBConnection($collection)
    {
        $date = new DateTime();
        $start_date = $date->format("y-m-d h:i:s");
        //Cambiar la url deseada de Mongo (Pueden ser incluidos los parámetros de user y password si asi se desea)
        try {
            $mongo = new \MongoDB\Client(MDB_CONN_STRING);
            $mongo_db = $mongo->selectDatabase('TestML');
            $collection = $mongo_db->$collection;
        } catch (MongoConnectionException $e) {
            $log = "Start Time: " . $start_date . PHP_EOL . "Error: Mongo Connection Failed".$e->getMessage();
            file_put_contents("log/errorlog-" . $start_date . ".log", $log, FILE_APPEND);
            die("MongoDB error: {$e->getMessage()}");
        } catch (Exception $e) {
            $log = "Start Time: " . $start_date . PHP_EOL . "Error: Mongo Connection Failed".$e->getMessage();
            file_put_contents("log/errorlog-" . $start_date . ".log", $log, FILE_APPEND);
            die("MongoDB error: {$e->getMessage()}");
        }
        //Regresa la conexión con la base de datos y colección seleccionada en Mongo
        return $collection;
    }

    //Función para obtener información de los libros disponibles en el catálogo de Mongo. Tiene las variables $connection para la conexión de Mongo y $limit para la cantidad de libros deseada (0 para todos)
    public static function books($connection, $limit)
    {
        $json_books = array();
        $cursor = $connection->find([], ['limit' => $limit]);
        foreach ($cursor as $document) {
            $json_books[] = $document;
        }
        //Regresa json linea por linea de los libros encontrados
        return $json_books;
    }

    //Función para encontrar un libro con el ISBN deseado. Tiene las variables $connection para la conexión de Mongo y $isbn para el numero de ISBN a buscar
    public static function findISBN($connection, $isbn)
    {
        $document = $connection->findOne(array("isbn" => $isbn));
        //Regresa el json del libro encontrado
        return json_encode($document);
    }

    //Función para encontrar un libro por el nombre. Tiene las variables $connection para la conexión de Mongo y $title para el título
    public static function findName($connection, $title)
    {
        $document = $connection->findOne(array("title.title" => $title));
        //Regresa el json del libro encontrado
        return json_encode($document);
    }

    //Función para la obtención del Array de un libro de acuerdo al ISBN. Tiene las variables $connection para la conexión de Mongo y $isbn para el numero de ISBN a buscar
    public static function bookArray($connection, $isbn)
    {
        $ml = new MLUtils();
        $book = $ml->findISBN($connection, $isbn);
        $book_array = json_decode($book, true);
        //Regresa el array del libro buscado por ISBN
        return $book_array;
    }

    //Función para obtener los libros de acuerdo al país que se busque. Tiene las variables $connection para la conexión de Mongo y $country para establecer el país a buscar.
    public static function findByCountry($connection, $country, $limit)
    {
        $json_books = array();
        $regex = new MongoDB\BSON\Regex ($country);
        $array = array("includedTerritoriality" => $regex);
        $cursor = $connection->find($array, ['limit' => $limit]);
        foreach ($cursor as $document) {
            $json_books[] = $document;
        }
        //Regresa json linea por linea de los libros encontrados
        return $json_books;
    }

    public static function categoryId($category)
    {
        $category_id = '';
        if (($handle = fopen("src/categories.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                if (strpos($data[4], $category["code"]) !== false) {
                    $category_id = $data[0];
                }
            }
            fclose($handle);
        }
        return $category_id;
    }
}
