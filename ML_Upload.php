<?php
require 'ML_Utils.php';
require 'src/upload_config.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$ml = new MLUtils();
$update_date = date("Y-m-d h:i:s");
$date = new DateTime();
$start_date = $date->format("y-m-d h:i:s");
$connection = $ml->mongoDBConnection('books');
$books = $ml->findByCountry($connection, COUNTRY, LIMIT);
foreach ($books as $book) {

    $array_book = json_decode(json_encode($book), true);

    //Availability Filter
    if ($array_book["availability"] == true) {
        //echo 'libro disponible';
    } else {
        //'libro no disponible, isbn ' . $array_book['isbn'];
        continue;
    }

    //Price Filter
    $precio = '';
    if (!empty($array_book['prices'])) {
        foreach ($array_book['prices'] as $price) {
            if ($price['currency'] == CURRENCY) {
                if ($price["type"] == PRICETYPE) {
                    $precio = $price['amount'];
                }
            }
        }
    } else {
        $log = "Start Time: " . $start_date . PHP_EOL . "Sin precio disponible en el país, no procede el artículo con isbn " . $array_book['isbn'];
        file_put_contents("log/errorlog-" . $start_date . ".log", $log, FILE_APPEND);
        continue;
    }

    //Format Filter
    if ($array_book["formatType"] == FORMATTYPE) {
    } else {
        $log = "Start Time: " . $start_date . PHP_EOL . "libro sin formato correcto, isbn " . $array_book['isbn'];
        file_put_contents("log/errorlog-" . $start_date . ".log", $log, FILE_APPEND);
        continue;
    }

    //Protection Filter
    if ($array_book["protectionType"] == PROTECTIONTYPE) {
    } else {
        $log = "Start Time: " . $start_date . PHP_EOL . "libro sin protección correcto, isbn " . $array_book['isbn'];
        file_put_contents("log/errorlog-" . $start_date . ".log", $log, FILE_APPEND);
        continue;
    }


    //Category Filter
    if (!empty($array_book['categories'])) {
        foreach ($array_book['categories'] as $category) {
            if ($category["identifier"] == "12") {
                if (!empty($ml->categoryId($category))) {
                    $category_id = '';
                    $category_id = $ml->categoryId($category);
                } else {
                    $category_id = "43";
                }
            }
        }
    } else {
        $category_id = "43";
    }

    //Author Filter
    if (!empty($array_book["contributors"])) {
        $autor = $array_book["contributors"][0]["name"] . ' ' . $array_book["contributors"][0]["lastName"];
    } else {
        $autor = 'Autor no disponible';
    }

    //Book Variables
    $isbn = $array_book['isbn'];
    $name = $array_book['title']['title'];
    $description = $array_book["synopsis"];
    $imprint = $array_book["imprintName"];
    $publisher = $array_book["imprintName"];
    $image = $array_book["image"];

    //Token Creation
    $ch = curl_init();
    $data = array("username" => APIUSERNAME, "password" => APIPWD);
    $data_string = json_encode($data);
    $ch = curl_init(UPLOADTOKENURL);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
    );
    $token = curl_exec($ch);
    $adminToken = json_decode($token);

    //Book Upload
    $sampleProductData = array(
        'sku' => $isbn,
        'name' => $name,
        'visibility' => 4,
        'type_id' => 'simple',
        'price' => $precio,
        'status' => 1,
        'attribute_set_id' => 10,
        'extension_attributes' => array(
            "stock_item" => array(
                'qty' => 999, 'is_in_stock' => 1, 'manage_stock' => 1, 'use_config_manage_stock' => 1, 'min_qty' => 1, 'use_config_min_qty' => 1, 'min_sale_qty' => 1, 'use_config_min_sale_qty' => 1, 'max_sale_qty ' => 10, 'use_config_max_sale_qty' => 1, 'is_qty_decimal' => 0, 'backorders' => 0, 'use_config_backorders' => 1, 'notify_stock_qty' => 1, 'use_config_notify_stock_qty' => 1
            ),
        ),
        'custom_attributes' => [
            ['attribute_code' => 'category_ids', 'value' => [$category_id]],
            ['attribute_code' => 'description', 'value' => $description],
            ['attribute_code' => 'autor_principal', 'value' => $autor],
            ['attribute_code' => 'formato', 'value' => 15],
            ['attribute_code' => 'genero', 'value' => 1],
            ['attribute_code' => 'titulo', 'value' => $name],
            ['attribute_code' => 'editorial', 'value' => $publisher],
            ['attribute_code' => 'sello', 'value' => $imprint],
            ['attribute_code' => 'isbn', 'value' => $isbn],
            ['attribute_code' => 'identificador_del_libro', 'value' => $isbn],
            ['attribute_code' => 'permisos_de_proteccion', 'value' => 1],
            ['attribute_code' => 'moneda', 'value' => CURRENCY],
            ['attribute_code' => 'libro_protegido', 'value' => "DRM" . PROTECTIONTYPE],
            ['attribute_code' => 'ultima_actualizacion', 'value' => $update_date],
        ],
    );
    $productData = json_encode(array('product' => $sampleProductData));
    $setHaders = array('Content-Type:application/json', 'Authorization:Bearer ' . $adminToken);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, UPLOADPRODUCTURL);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $productData);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $setHaders);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);

    //Token Creatio
    $image_url = UPLOADURL . "rest/v1/products/$isbn/media";
    $ch = curl_init();
    $data = array("username" => APIUSERNAME, "password" => APIPWD);
    $data_string = json_encode($data);
    $ch = curl_init(UPLOADTOKENURL);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
    );
    $token = curl_exec($ch);
    $adminToken = json_decode($token);

    //Image Upload
    $b64image = base64_encode(file_get_contents($image));
    $sampleImageData = array(
        "media_type" => "image",
        "label" => "Image",
        "position" => 0,
        "disabled" => false,
        "types" => ["image", "small_image", "thumbnail"
        ],
        "file"=>$name.".jpg",
        "content"=>[
            "base64_encoded_data"=>$b64image,
            "type"=>"file/jpg",
            "name"=>$name.".jpg"
        ]

    );
    $imageData = json_encode(array('entry' => $sampleImageData));
    $setHaders = array('Content-Type:application/json', 'Authorization:Bearer ' . $adminToken);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $image_url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $imageData);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $setHaders);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    curl_close($ch);

}

